﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EstudioExa.DataPersona
{
    public class DatPersona
    {
        //METODO
        public DataTable Obtener()//Este metodo regresa una Tabla
        {
            DataTable dt = new DataTable();//Objeto DataTable declara que voy a utilizar una tabla vacia al momento
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);//De donde voy a obtener la informacion de la tabla
            string query = "select ID, Nombre, Fecha_Nacimiento, Correo, Usuario, Contraseña from [dbo].[Persona]";//Cadena de coneccion
            SqlDataAdapter da = new SqlDataAdapter(query, con);//SqlDataAdapter abre la coneccion ejecuta el query de la direccion SqlClient y lo guarde la informacion en memoria "da"
            da.Fill(dt);//nos dice que la informacion en memoria lo llena y lo almacen en nuestro DataTable "dt"
            return dt;//Regresa la tabla
        }

        public int Agregar(string Nomb, DateTime Fecha, string Mail, string Usuaurio, string Pass)//Regresa un entero por que agrega un ID
        {
            int filasafectadas = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);//De donde voy a obtener la informacion de la tabla
            string query = string.Format("insert into [dbo].[Persona] (Nombre, Fecha_Nacimiento, Correo, Usuario, Contraseña) values ('{0}', '{1}', '{2}', '{3}', {4})", Nomb, Fecha.ToString("yyyy/MM/dd"), Mail, Usuaurio, Pass);
            SqlCommand com = new SqlCommand(query, con);//quien va a ejecutar todo
            try
            {
                con.Open();//Abre coneccion
                filasafectadas = com.ExecuteNonQuery();//que vamos a llenar ExecuteNonQuerynos indica que vamos a ejecutar todo lo anterior 
                con.Close();//Se cierra coneccion
                return filasafectadas;
            }
            catch (Exception ex)// Si llega haber una excepcion...
            {
                con.Close();// Cierró coneccion
                throw ex;//lleva al meto de accion que esta ejecutando
            }
        }

        public int actualizar(string nomb, DateTime fech, string correo, string usuario, string pass, int id)
        {
            int filas = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["sql"].ConnectionString);
            string query = string.Format("update [dbo].[Persona] set Nombre = '{0}', Fecha_Nacimiento = '{1}', Correo = '{2}', Usuario = '{3}', Contraseña = '{4}' where ID = {5}", nomb, fech.ToString("yyyy/MM/dd"), correo, usuario, pass, id);
            SqlCommand com = new SqlCommand(query, con);
            try
            {
                con.Open();
                filas = com.ExecuteNonQuery();
                con.Close();
                return filas;
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
        }

        public int borrar(int id)
        {
            int filasafectadas = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["sql"].ConnectionString);
            string query = string.Format("delete from [dbo].[Persona] where id = {0}", id);
            SqlCommand com = new SqlCommand(query, con);
            try
            {
                con.Open();
                filasafectadas = com.ExecuteNonQuery();
                con.Close();
                return filasafectadas;
            }
            catch (Exception ex)
            {
                con.Close();
                throw ex;
            }
        }

        public DataRow Editar(int id)//Este metodo regresa una fila
        {
            DataTable dt = new DataTable();//Objeto DataTable declara que voy a utilizar una tabla vacia al momento
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);//De donde voy a obtener la informacion de la tabla
            string query = "select ID, Nombre, Fecha_Nacimiento, Correo, Usuario, Contraseña from [dbo].[Persona] where ID = " + id;//Cadena de coneccion
            SqlDataAdapter da = new SqlDataAdapter(query, con);//SqlDataAdapter abre la coneccion ejecuta el query de la direccion SqlClient y lo guarde la informacion en memoria "da"
            da.Fill(dt);//nos dice que la informacion en memoria lo llena y lo almacen en nuestro DataTable "dt"
            return dt.Rows[0];//Regresa la fila
        }
    }
} 