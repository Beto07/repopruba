﻿using EstudioExa.BissinessPersona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EstudioExa.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BtnMostrar()
        {
            try
            {
                BusPersona p = new BusPersona();
                return View(p.Obtener());
            }
            catch (Exception ex)
            {

                TempData["mensaje"] = ex.Message;
                return View("Index");
            }
        }

        public ActionResult BtnAgregar()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                TempData["mensaje"] = ex.Message;
                return View();
            }
        }

        public ActionResult Agregar(EntPersona pe)
        {
            try
            {
                BusPersona p = new BusPersona();
                p.Agregar(pe);

                TempData["mensaje"] = "Se Agregó persona" + pe.Nombre;
                return RedirectToAction("BtnMostrar");
            }
            catch (Exception ex)
            {
                TempData["mensaje"] = ex.Message;
                return View();
            }
        }

        public ActionResult BtnEliminar()
        {
            try
            {
                BusPersona p = new BusPersona();
                return View(p.Obtener());
            }
            catch (Exception ex)
            {

                TempData["mensaje"] = ex.Message;
                return View("Index");
            }
        }

        public ActionResult Eliminar(EntPersona pe)
        {
            try
            {
                BusPersona p = new BusPersona();
                p.Borrar(pe.Id);

                TempData["mensaje"] = "Se borró persona" + pe.Nombre;
                return RedirectToAction("BtnMostrar");
            }
            catch (Exception ex)
            {
                TempData["mensaje"] = ex.Message;
                return View();
            }
        }

       public ActionResult BtnEditar()
        {
            try
            {
                BusPersona p = new BusPersona();
                return View(p.Obtener());
            }
            catch (Exception ex)
            {
                TempData["mensaje"] = ex.Message;
                return View("index");
            }
        }

       public ActionResult Editar(int Id)   
       {
           try
           {
               BusPersona p = new BusPersona();
               return View(p.Editar(Id));
           }
           catch (Exception ex)
           {
               TempData["mensaje"] = ex.Message;
               return View("index");
           }
       }

       public ActionResult Actualizar(EntPersona pe)
       {
           try
           {
               BusPersona p = new BusPersona();
               p.Actualizar(pe);
               TempData["mensaje"] = "Se actualizó" + pe.Nombre; 
               return View("BtnMostra", p.Obtener());
           }
           catch (Exception ex)
           {
               TempData["mensaje"] = ex.Message;
               return View("index");
           }
       }
    }
}