﻿using EstudioExa.DataPersona;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace EstudioExa.BissinessPersona
{
    public class BusPersona
    {
        //DatPersona es la clase y new DatPersona es el constructor
        DatPersona p = new DatPersona();//Instancia de la clase nombre de la clase de manera global

        public List<EntPersona> Obtener()//Listas genericas de nuestra entidad
        {
            
            
            List<EntPersona> lst = new List<EntPersona>();
            DataTable dt = p.Obtener(); //Recupera el dataTable que tenemos en la capa de datos

            foreach (DataRow r in dt.Rows)
            {
                EntPersona pe = new EntPersona();//intancia para tener acceso a todos los elementos

                pe.Contraseña = r["Contraseña"].ToString();
                pe.Correo = r["Correo"].ToString();
                pe.Fecha_Nacimiento = Convert.ToDateTime(r["Fecha_Nacimiento"]);
                pe.Id =Convert.ToInt32(r["Id"]);
                pe.Nombre = r["Nombre"].ToString();
                pe.Usuario = r["Usuario"].ToString();

                lst.Add(pe);
            }
            return lst; 
        }

        //Estos metodos solo hacen las validaciones
        public void Agregar(EntPersona pe)
        {            
            int filas = 0;
            //string Nomb, string Fecha, string Mail, string Usuaurio, string Pass
            filas = p.Agregar(pe.Nombre, pe.Fecha_Nacimiento, pe.Correo, pe.Usuario, pe.Contraseña);

            if (filas != 1)
            {
                throw new ApplicationException("No se agrego la persona" + pe.Nombre);
            }
        }

        public void Actualizar(EntPersona pe)
        {
            int filas = 0;
            filas = p.actualizar(pe.Nombre,pe.Fecha_Nacimiento, pe.Correo, pe.Usuario, pe.Contraseña, pe.Id);

            if (filas != 1)
            {
                throw new ApplicationException("No se Actualizó  la persona" + pe.Nombre);
            }
        }

        public void Borrar(int id)
        {
            int filas = 0;
            filas = p.borrar(id);

            if (filas != 1)
            {
                throw new ApplicationException("se borró  la persona");
            }
        }

        public EntPersona Editar(int id) 
        {
            DataRow r = p.Editar(id);

            EntPersona pe = new EntPersona();

            pe.Contraseña = r["Contraseña"].ToString();
            pe.Correo = r["Correo"].ToString();
            pe.Fecha_Nacimiento = Convert.ToDateTime(r["Fecha_Nacimiento"]);
            pe.Id = Convert.ToInt32(r["Id"]);
            pe.Nombre = r["Nombre"].ToString();
            pe.Usuario = r["Usuario"].ToString();

            return pe;
        }

    }
}