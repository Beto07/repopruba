﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EstudioExa.BissinessPersona
{
    public class EntPersona
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime Fecha_Nacimiento { get; set; }
        public string Correo { get; set; }
        public string Usuario { get; set; }

        public string contraseña;

        public string Contraseña
        {
            get {
                 if (contraseña == "" || contraseña == null)
                 {
                     contraseña = "N/A";                    
                 }
                  return contraseña; }
            set { contraseña = value; }
        }
        
       
        //propfull + tab, tab para poder haceruna evaluacion dentro de las entidades
        private string InfoUsuario;

        public string infousuario
        {
            get {
                 InfoUsuario = string.Format("Usuario: {0}  - Contraseña: {1}", Usuario, Contraseña);// se trabaja con private
                 return InfoUsuario; 
                }

            set { InfoUsuario = value; }
        }
        
    }
}